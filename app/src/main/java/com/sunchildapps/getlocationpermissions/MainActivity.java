package com.sunchildapps.getlocationpermissions;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.util.TimeZone;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.audiofx.BassBoost;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private LocationManager locationManager;
    private LocationListener locationListener;
    private String FORECAST_BASE_URL = "api.forecast.io";
    private String API_KEY = "853e33a2a5d7d68a2cb9c126833b5a87";
    private Uri.Builder mUri;

    String Today;
    TextView day1Name, day2Name, day3Name, day4Name, day5Name, day6Name, TODAY;
    ImageView day1I, day2I, day3I, day4I, day5I, day6I, TODAY_ICON;
    TextView day1Temp, day2Temp, day3Temp, day4Temp, day5Temp, day6Temp, TODAY_TEMP, TODAY_HUMIDITY, TODAY_WINDIR,TODAY_WINDSPEED;

    ActionBar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();
        toolbar.setTitle("");
        TODAY = (TextView)findViewById(R.id.textView2);
        TODAY_TEMP = (TextView)findViewById(R.id.tv_todaytemp);
        TODAY_ICON = (ImageView)findViewById(R.id.imageView3);
        TODAY_HUMIDITY = (TextView)findViewById(R.id.tv_humidity);
        TODAY_WINDIR = (TextView)findViewById(R.id.tv_winddirection);
        TODAY_WINDSPEED = (TextView)findViewById(R.id.tv_wind);

        day1Name = (TextView)findViewById(R.id.textView4);
        day2Name = (TextView)findViewById(R.id.textView5);
        day3Name = (TextView)findViewById(R.id.textView6);
        day4Name = (TextView)findViewById(R.id.textView7);
        day5Name = (TextView)findViewById(R.id.textView8);
        day6Name = (TextView)findViewById(R.id.textView9);

        day1I = (ImageView)findViewById(R.id.imageView5);
        day2I = (ImageView)findViewById(R.id.imageView6);
        day3I = (ImageView)findViewById(R.id.imageView7);
        day4I = (ImageView)findViewById(R.id.imageView8);
        day5I = (ImageView)findViewById(R.id.imageView9);
        day6I = (ImageView)findViewById(R.id.imageView10);

        day1Temp = (TextView)findViewById(R.id.textView10);
        day2Temp = (TextView)findViewById(R.id.textView11);
        day3Temp = (TextView)findViewById(R.id.textView12);
        day4Temp = (TextView)findViewById(R.id.textView13);
        day5Temp = (TextView)findViewById(R.id.textView14);
        day6Temp = (TextView)findViewById(R.id.textView15);



        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                //Log.e("Latitud", "" + location.getLatitude());
                //Log.w("Longitud", "" + location.getLongitude());
                mUri = new Uri.Builder();
                mUri.scheme("https")
                        .authority(FORECAST_BASE_URL)
                        .appendPath("forecast")
                        .appendPath(API_KEY)
                        .appendPath(String.valueOf(location.getLatitude())+","+String.valueOf(location.getLongitude()));

                String myUrl = mUri.build().toString();
                //Log.v("URL", myUrl);
                sendRequest(MainActivity.this, myUrl);


            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);


            }
        };
        //permissions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET
                }, 10);
                return;
            }
        } else {
            locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
                return;
        }
    }

    public void sendRequest(Context context, String URL) {
        RequestQueue queue = Volley.newRequestQueue(context);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.w("Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String location = jsonObject.getString("timezone");
                            String today = jsonObject.getString("currently");
                            //Log.e("Hoy---", today);

                            JSONObject jsonToday = new JSONObject(today);
                            String dateToday = jsonToday.getString("time");//get today time
                            String iconToday = jsonToday.getString("icon");
                            String tempToday = jsonToday.getString("temperature");
                            String humidityToday =  jsonToday.getString("humidity");
                            String windSpeedToday = jsonToday.getString("windSpeed");
                            String windbearingToday = jsonToday.getString("windBearing");

                            String week = jsonObject.getString("daily");
                            //Log.v("Toda la semana", week);

                            JSONObject jsonWeek = new JSONObject(week);
                            String data = jsonWeek.getString("data");
                            //Log.v("data", data);

                            JSONArray resultArray = jsonWeek.getJSONArray("data");//whole week
                            String[] dateAll = new String[resultArray.length()];
                            String[] iconAll = new String[resultArray.length()];
                            String[]tempAll  = new String[resultArray.length()];
                            String[]humidityAll  = new String[resultArray.length()];
                            String[] windspAll  = new String[resultArray.length()];
                            String[]windBAll  = new String[resultArray.length()];

                            for (int i=0 ; i<resultArray.length() ; i++){
                                JSONObject objectAll = resultArray.getJSONObject(i);
                                dateAll[i]= objectAll.getString("time");
                                iconAll[i]= objectAll.getString("icon");
                                tempAll[i]= objectAll.getString("temperatureMax");
                                humidityAll[i]= objectAll.getString("humidity");
                                windspAll[i]= objectAll.getString("windSpeed");
                                windBAll[i]= objectAll.getString("windBearing");
                            }

                            ForecastAdapter forecastAdapter = new ForecastAdapter(MainActivity.this,dateAll,iconAll,tempAll,humidityAll,windspAll,
                                    windBAll, tempToday,iconToday,dateToday,humidityToday,windSpeedToday,windbearingToday,location);

                            forecastAdapter.setData();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.w("Error", error);
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);


    }

    public class ForecastAdapter {

        Context context;
        String[] date;
        String[] icon;
        String[]temperature;
        String[]humidity;
        String[] windspeed;
        String[]windB;

        String tempToday,iconToday,dateToday,humidityToday,windSpeedToday,windbearingToday, location;

        public ForecastAdapter(Context context, String[]date,String[] icon,String[]temperature,  String[] humidity,
                               String[]windspeed, String[]windB,String tempToday,String iconToday,String dateToday,
                               String humidityToday,String windSpeedToday,String windbearingToday,String location){

            this.date = date;
            this.icon=icon;
            this.temperature=temperature;
            this.windspeed = windspeed;
            this.humidity= humidity;
            this.windspeed = windspeed;
            this.windB = windB;
            this.context=context;
            this.tempToday = tempToday;
            this.iconToday = iconToday;
            this.dateToday = dateToday;
            this.humidityToday = humidityToday;
            this.windSpeedToday = windSpeedToday;
            this.windbearingToday = windbearingToday;
            this.location= location;
        }

        public void setData(){

            long[] dateLong = new long[date.length];
            for (int i=0;i<date.length;i++){
                dateLong[i] = Long.parseLong(date[i]);
                java.util.Date time2 = new java.util.Date(dateLong[i]*1000);
                date[i]= time2.toString().substring(0,3);
            }

            SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE, MMM d, yyyy");
            toolbar.setTitle(location);
            //Today=  dayFormat.format(Long.parseLong(dateToday)*1000);
            TODAY.setText(dayFormat.format(Long.parseLong(dateToday)*1000));
            TODAY_TEMP.setText(tempToday);
            TODAY_ICON.setImageResource(setIcons(iconToday));
            TODAY_WINDSPEED.setText(windSpeedToday);
            TODAY_HUMIDITY.setText(humidityToday);
            TODAY_WINDIR.setText(setWindDirectio(windbearingToday));


            day1Name.setText(date[1]);
            day2Name.setText(date[2]);
            day3Name.setText(date[3]);
            day4Name.setText(date[4]);
            day5Name.setText(date[5]);
            day6Name.setText(date[6]);

            day1I.setImageResource(setIcons(icon[1]));
            day2I.setImageResource(setIcons(icon[2]));
            day3I.setImageResource(setIcons(icon[3]));
            day4I.setImageResource(setIcons(icon[4]));
            day5I.setImageResource(setIcons(icon[5]));
            day6I.setImageResource(setIcons(icon[6]));

            day1Temp.setText(temperature[1]);
            day2Temp.setText(temperature[2]);
            day3Temp.setText(temperature[3]);
            day4Temp.setText(temperature[4]);
            day5Temp.setText(temperature[5]);
            day6Temp.setText(temperature[6]);
        }
    }

    private int setIcons(String iconId){
        switch(iconId){
            case "clear-day":
                return R.drawable.sun;
            case "clear-night":
                return R.drawable.moon_full;
            case "rain":
                return R.drawable.cloud_rain;
            case "snow":
                return R.drawable.cloud_snow;
            case "sleet":
                return  R.drawable.cloud_snow_alt;
            case "wind":
                return R.drawable.cloud_wind;
            case "fog":
                return R.drawable.cloud_fog_alt;
            case "cloudy":
                return R.drawable.cloud_download;
            case "partly-cloudy-day":
                return R.drawable.cloud_sun;
            case "partly-cloudy-night":
                return R.drawable.cloud_moon;
        }
        return -1;
    }

    private String setWindDirectio(String windDirection){
        long winDire = Long.parseLong(windDirection);
        String direction = "";

        if (winDire >= 337.5 || winDire < 22.5) {
            direction = "N";
        } else if (winDire >= 22.5 && winDire < 67.5) {
            direction = "NE";
        } else if (winDire >= 67.5 && winDire < 112.5) {
            direction = "E";
        } else if (winDire >= 112.5 && winDire < 157.5) {
            direction = "SE";
        } else if (winDire >= 157.5 && winDire < 202.5) {
            direction = "S";
        } else if (winDire >= 202.5 && winDire < 247.5) {
            direction = "SW";
        } else if (winDire >= 247.5 && winDire < 292.5) {
            direction = "W";
        } else if (winDire >= 292.5 && winDire < 337.5) {
            direction = "NW";
        }
        return direction;
    }



}
